#!/usr/bin/env bash


 mkdir -p ~/.dwall


input="/home/${USER}/.dwall/dwall-i3.txt"
while IFS= read -r line
do
  /usr/share/dynamic-wallpaper/dwall.sh -p -s  "$line"
done < "$input"
