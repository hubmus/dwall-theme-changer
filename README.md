## Dwall-theme-changer  [ARCHCRAFT-I3]



__Rofi dwall-menu to choose a dwall [heic] theme before reboot or shutdown.__

_The wallpaper is changing with the time of the day. [24/7]_

###### The menu is meant for Archcraft-i3 version, but can be used for other DE/WM too, using crontab.

- [ ] In the i3 config i have [exec_always --no-startup-id  sh /home/user/.config/i3/rofi/bin/i3-dwall-set.sh]


- [ ] [dwall-menu] creates a txt file and print the name of the chosen theme in the file.

- [ ] [i3-dwall-set.sh] uses this chosen theme from the txt file, for the next boot.

- [ ] [.dwall] Dir will be created in the home dir, if not excist.

I changed the default powermenu a wee bit. It reboots or shutdown straight after enter.



### INFO Archcraft & Dwall ...

- [ArchCraft Main](https://archcraft.io/) ---> for downloading and installing Archcraft.

- [Dwall Github](https://github.com/adi1090x/dynamic-wallpaper) ---> for installing dwall.

- [More wallpapers (large files)](https://gitlab.com/hubmus/dwall-extra-wallpapers)

---

### modified menu. restart dwall in session. 

![dwall-menu-2](/uploads/62ce284a0b68b15dfddf65bc879c1d38/dwall-menu-2.gif)

![Screenshot_2021-12-16_15-28-46](/uploads/fe0576b4c84f06d22ae939fa08ea7d52/Screenshot_2021-12-16_15-28-46.png) 

![Screenshot_2021-12-16_16-12-27](/uploads/ba35d0be3d07f68a3d6682cfee26f714/Screenshot_2021-12-16_16-12-27.png)


##### Source github adi1090x/dynamic-wallpaper

[adi1090x/github/personal/files](https://github.com/adi1090x/files/tree/master/dynamic-wallpaper/wallpapers)
